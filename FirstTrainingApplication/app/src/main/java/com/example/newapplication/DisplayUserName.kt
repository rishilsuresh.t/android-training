package com.example.newapplication

import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_display_user_name.*


class DisplayUserName : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_user_name)
        val message = intent.getStringExtra(EXTRA_MESSAGE)
        displayName.text = message
    }
}
