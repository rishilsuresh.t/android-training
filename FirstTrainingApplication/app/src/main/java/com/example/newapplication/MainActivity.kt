package com.example.newapplication

import android.content.Intent
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun authentication(view: View) {
        val userNameRequired: String = getString(R.string.userNameRequired)
        val passwordRequired: String = getString(R.string.passwordRequired)
        val validPassword: String = getString(R.string.validPassword)
        val validUserName: String = getString(R.string.validUserName)
        val notValidUser: String = getString(R.string.notValidUser)
        val userNameText: String = userName.text.toString()
        val passwordText: String = password.text.toString()

        if (userNameText.isEmpty() && passwordText.isEmpty()) {
            userName.error = userNameRequired
            password.error = passwordRequired
        } else if (passwordText.isEmpty()) {
            password.error = passwordRequired
        } else if (userNameText.isEmpty()) {
            userName.error = userNameRequired
        } else {
            if (passwordText == validPassword && userNameText == validUserName) {
                startActivity(Intent(this, DisplayUserName::class.java).apply {
                    putExtra(EXTRA_MESSAGE, userNameText)
                })
            } else {
                Toast.makeText(applicationContext,  notValidUser, Toast.LENGTH_LONG).show()
            }
        }
    }
}
